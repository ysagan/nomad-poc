job "job-name" {
  datacenters = ["dc1"]
  type = "batch"

  periodic {
    cron = "* * * * * *"
    prohibit_overlap = true
  }

  group "job-group" {
    task "job-task" {
      driver = "raw_exec"

      config {
        command = "/usr/bin/php"
        args = [
          "/vagrant/php/job.php",
        ]
      }

      resources {
        # defaults
      }
    }
  }
}