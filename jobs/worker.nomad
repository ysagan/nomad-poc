job "worker-name" {
  datacenters = ["dc1"]
  type = "service"

  group "worker-group" {
    count = 5

    task "worker-task" {
      driver = "raw_exec"

      config {
        command = "/usr/bin/php"
        args = [
          "/vagrant/php/worker.php",
        ]
      }

      resources {
        # defaults
      }
    }
  }
}