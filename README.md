Prerequisites:

* Install virtualbox, vagrant, ansible
* Execute **`./preinstall`** to check/install vagrant-hosts plugin and put needed hosts into your /etc/hosts 
* Run **`vagrant up`** to download, provision and start a nomad cluster of three

Check:

* http://nomad1.local:3000/
* http://nomad1.local:8500/

See php folder for examples of batch and service scripts.

See jobs folder for examples of nomad jobs.

Follow official nomad [docs](https://www.nomadproject.io/docs/index.html), [guides](https://www.nomadproject.io/guides/index.html) and play around with the cluster.